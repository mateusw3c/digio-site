window.Widgets = {}

Widgets.mediumFeeds = {

    resultsLoader: '*[data-scope="medium-feed"]:first',

    mediumUrl: function() {
        return 'https://exec.clay.run/mateusw3c/medium-get-users-posts?username=meudigio';
    },

    fetch: function() {
        return $.ajax({
            url: Widgets.mediumFeeds.mediumUrl(),
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                Widgets.mediumFeeds.proccessResponse(data);

                setTimeout(function(){
                    DIGIO.sliderMedium();
                }, 100);
            },
            error: function(error) {
                var resultsLoader, ul;
                resultsLoader = $(Widgets.mediumFeeds.resultsLoader);
                resultsLoader.html('<ul class="list-posts"></ul>');
                ul = resultsLoader.find('ul:first');
                return ul.append(Widgets.mediumFeeds.failureMsg());
            },
            complete: function() {
                setTimeout(function(){
                    $(Widgets.mediumFeeds.resultsLoader).removeClass('loading-content');
                }, 1000);
            }
        });
    },

    proccessResponse: function(data) {

        var results, resultsLoader, ul,
            listPosts = data.payload.references.Post;
            user      = data.payload.user;

        resultsLoader = $(Widgets.mediumFeeds.resultsLoader);
        resultsLoader.html("<ul class='list-posts'></ul>");
        ul = resultsLoader.find('ul:first');

        if (data.success === true) {
            results = [];

            for (i in listPosts) {
                var post = listPosts[i];
                results.push(ul.append(Widgets.mediumFeeds.buildEntryHTML(post, user)));
            }

            return results;
        } else {
            return ul.append(Widgets.mediumFeeds.failureMsg());
        }
    },

    failureMsg: function() {
        return '<li class="empty">Ocorreu um erro ao carregar o conteúdo.</li>';
    },

    roundNumber: function(number, precision) {
        var factor = Math.pow(10, precision);
        var tempNumber = number * factor;
        var roundedTempNumber = Math.round(tempNumber);
        return roundedTempNumber / factor;
    },

    buildEntryHTML: function(post, user) {
        var readTime      = Widgets.mediumFeeds.roundNumber(post.virtuals.readingTime, 1),
            clapCount     = Widgets.mediumFeeds.roundNumber(post.virtuals.totalClapCount, 1),
            responseCount = Widgets.mediumFeeds.roundNumber(post.virtuals.responsesCreatedCount, 1),
            html;

        return html = '<li class="post">\
            <a target="_blank" href="https://medium.com/@' + user.username + '/' + post.id + '" title="' + post.title + '">\
                <div class="post-image" style="background-image: url(https://cdn-images-1.medium.com/fit/t/800/240/' + post.virtuals.previewImage.imageId + ');"></div>\
                <div class="post-content">\
                    <h6>' + post.title + '</h6>\
                    <div class="post-details">\
                        <div class="row">\
                            <div class="col-md-5 col-sm-5 col-xs-12">\
                                <div class="post-author">\
                                    <div class="group-image-author">\
                                        <img src="https://cdn-images-1.medium.com/fit/c/60/60/' + user.imageId + '" alt="' + user.name + '" />\
                                    </div>\
                                    <strong>' + user.name + '</strong>\
                                    <i>' + readTime + ' min read</i>\
                                </div>\
                            </div>\
                            <div class="col-md-7 col-sm-7 col-xs-12">\
                                <ul class="list-counters">\
                                    <li>\
                                        <i class="icon-clap"></i>\
                                        <span class="count">' + clapCount + '</span>\
                                    </li>\
                                    <li class="divisor"></li>\
                                    <li>\
                                        <i class="icon-response"></i>\
                                        <span class="count">' + responseCount + '</span>\
                                    </li>\
                                </ul>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            </a>\
        </li>';
    }
};