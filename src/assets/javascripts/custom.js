var DIGIO = DIGIO || {};

$(document).ready(function() {
	DIGIO.init();
});

$(window).resize(function() {
});

$(document).keyup(function(e) {
	// ESC Close Sidebar
	if (e.keyCode == 27) {
		DIGIO.closeSidebar();
	}
});

DIGIO.init = function() {
	this.sliderHome();
	this.sliderProducts();
	this.sliderTestimonials();
	this.sliderPartners();
	this.sliderAppDigio();
	this.accordion();
	this.toggleSidebar();
	this.validateForms();
	this.submitForms();
	this.fixedMenu();
	this.anchorNavigation();
	this.getDigioVideo();
	this.calcFormSimulation();
	this.mobile_menu();
	this.masks();
};

DIGIO.sliderHome = function() {

	var $scope = $('*[data-scope="list-slider-home"]:first');

	$scope.slick({
		fade: true,
		dots: true,
		speed: 100,
		arrows: false,
		infinite: true,
		cssEase: 'linear',
		draggable: false,
		autoplay: true,
		autoplaySpeed: 4000,
		customPaging: function (slider, i) {
			return '<a>0'+ (i + 1) +'</a>';
		},
		responsive: [
			{
				breakpoint: 768,
				settings: {
					adaptiveHeight: true
				}
			}
		]
	});

};

DIGIO.sliderProducts = function() {

	var $scope = $('*[data-scope="list-slider-products"]:first');

	$scope.slick({
		fade: true,
		dots: false,
		speed: 100,
		arrows: true,
		infinite: true,
		adaptiveHeight: true,
		draggable: true,
		// autoplay: true,
		// autoplaySpeed: 4000,
		prevArrow: '<i class="icon-arrow-left-circle"></i>',
		nextArrow: '<i class="icon-arrow-right-circle"></i>'
	});

};

DIGIO.sliderTestimonials = function() {

	var $scope = $('*[data-scope="list-slider-testimonials"]:first');

	$scope.slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		speed: 300,
		arrows: true,
		infinite: true,
		draggable: true,
		vertical: true,
		cssEase: 'ease-in-out',
		prevArrow: $('*[data-arrow="slider-testimonials-top"]:first'),
		nextArrow: $('*[data-arrow="slider-testimonials-bottom"]:first'),
		responsive: [
			{
				breakpoint: 768,
				settings: {
					fade: true,
					slidesToShow: 1,
					arrows: false,
					dots: true,
					vertical: false,
					adaptiveHeight: true
				}
			}
		]
	});

};

DIGIO.sliderMedium = function() {

	var $scope = $('*[data-scope="medium-feed"]:first').find('ul:first');

	$scope.slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		centerMode: true,
		dots: true,
		speed: 300,
		arrows: false,
		infinite: true,
		draggable: true,
		variableWidth: true,
		adaptiveHeight: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});

};

DIGIO.sliderPartners = function() {

	var $scope         = $('*[data-scope="partners"]:first');
	var $slider1       = $('*[data-item="slider-1"]:first', $scope);
	var $slider2       = $('*[data-item="slider-2"]:first', $scope);
	var $controllers   = $('*[data-item="controllers"]:first', $scope).find('a');

	$controllers.on( 'click' , function(e) {
		e.preventDefault();

		var slide = $(this).data('slide');
		$slider1.slick('slickGoTo', slide);
		$slider2.slick('slickGoTo', slide);

		$controllers.parent().removeClass('current-slide');
		$(this).parent().addClass('current-slide');
	});

	$slider1.slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		draggable: false,
		adaptiveHeight: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					dots: true,
					draggable: false,
				}
			}
		]
	});

	$slider2.slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		draggable: false
	});

};

DIGIO.sliderAppDigio = function() {

	var $scope = $('*[data-scope="list-slider-app"]:first');

	$scope.slick({
		fade: true,
		dots: false,
		speed: 100,
		arrows: false,
		infinite: true,
		cssEase: 'linear',
		draggable: false,
		autoplay: true,
		autoplaySpeed: 7000
	});

};

DIGIO.accordion = function() {

	var $scope   = $('*[data-scope="accordion"]:first'),
		$toggle = $('*[data-item="toggle"]', $scope),
		$content = $('*[data-item="content"]', $scope);

	$toggle.on( 'click' , function(e) {
		e.preventDefault();

		$(this).next().slideToggle(200);
		$(this).parent().toggleClass('opened');
	});

};

DIGIO.toggleSidebar = function() {
	var $scope     = $('*[data-scope="sidebar-form"]:first'),
		$open      = $('*[data-item="open-sidebar"]'),
		$close     = $('*[data-item="close"]', $scope);

	$open.on( 'click' , function(e) {
		e.preventDefault();
		$scope.addClass('active-form');

		setTimeout(function(){
			$scope.find('input:first').focus();
		}, 100);
	});

	$close.on( 'click' , function(e) {
		e.preventDefault();
		DIGIO.closeSidebar();
	});
};

DIGIO.closeSidebar = function() {
	var $scope   = $('*[data-scope="sidebar-form"]:first');
	$scope.removeClass('active-form');
};

DIGIO.validateForms = function() {
	var $registerForm = $('*[data-scope="register-form"]:first'),
		$contactForm = $('*[data-scope="contact-form"]:first');

	$registerForm.validate({
		rules: {
			nome: 'required',
			email: {
				required: true,
				email: true
			},
			confirmaEmail: {
				required: true,
				equalTo: '#email'
			},
			cpf: {
				required: true
			},
			celular: {
				required: true
			},
			dataNascimento: {
				required: true
			}
		},
		messages: {
			nome: 'Nome é obrigatório',
			email: {
				required: 'Email é obrigatório',
				email: 'Insira um email válido'
			},
			confirmaEmail: {
				required: 'Confirmar email é obrigatório',
				email: 'Insira um email válido',
				equalTo: 'Seu email não confere com o digitado acima'
			},
			cpf: {
				required: 'CPF é obrigatório',
				minlength: 'Seu CPF precisa ter 11 caracteres',
				maxlength: 'Seu CPF precisa ter 11 caracteres',
				number: 'Digite apenas números'
			},
			celular: {
				required: 'Celular é obrigatório',
				minlength: 'Seu Telefone precisa ter 11 ao menos caracteres',
				number: 'Digite apenas números'
			},
			dataNascimento: {
				required: 'Data de nascimento é obrigatório',
			}
		}
	});

	$contactForm.validate({
		rules: {
			nome: 'required',
			email: {
				required: true,
				email: true
			},
			mensagem: 'required'
		},
		messages: {
			nome: 'Nome é obrigatório',
			email: {
				required: 'Email é obrigatório',
				email: 'Insira um email válido'
			},
			mensagem: {
				required: 'Mensagem é obrigatória',
			}
		}
	});
};

DIGIO.submitForms = function() {
	var $registerForm  = $('*[data-scope="register-form"]:first'),
		$success       = $('*[data-item="form-success"]:first'),
		$buttonSubmit  = $('*[data-item="button-submit"]:first', $registerForm);

	$registerForm.on('submit', function(e) {
		e.preventDefault();

		if($registerForm.valid()){
			console.log('Form válido');

			// Fake success form
			$registerForm.hide();
			$success.fadeIn();

			// Request Ajax
			// $.ajax({
			// 	url: $(this).data('js-api-url'),
			// 	type: "POST",
			// 	dataType: "json"
			// 	data: JSON.stringify(_params),
			// 	crossDomain: !0,
			// 	success: function (resp) {
			// 		alert('Sucesso');
			// 	},
			// 	error: function (resp) {
			// 		alert('Erro');
			// 	}
			// });

			return;
		}

		console.log('Form inválido');
	});

};

DIGIO.fixedMenu = function() {
	var $scope = $('*[data-scope="header"]:first'),
		$fixedElement = false;

	$(window).on('scroll', function(){
		if($(document).scrollTop() > 400){
			if ($fixedElement == false) {
				$scope.addClass('header-fixed');
			}
			$fixedElement = true;
		} else {
			if ($fixedElement == true) {
				$scope.removeClass('header-fixed');
			}
			$fixedElement = false;
		}
	});
}

DIGIO.anchorNavigation = function() {
	var $scope = $('*[data-scope="anchor-navigation"]:first'),
		$link = $('a', $scope),
		$anchor,
		$href;

	$link.on( 'click' , function(e) {
		e.preventDefault();

		href = $(this).attr('href');
		anchor = $(href).offset().top;

		$('html, body').stop().animate({
			scrollTop: anchor
		}, 700);
	});
}

DIGIO.getDigioVideo = function() {
	var $scope     = $('*[data-scope="digio-video"]:first'),
		$container = $('*[data-item="container-video"]:first', $scope),
		$link      = $('*[data-item="link"]:first', $scope);

	$link.on( 'click' , function(e) {
		e.preventDefault();

		var videoUrl = $(this).attr('href'),
			videoId = videoUrl.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);

		$container.html('<iframe src="https://www.youtube.com/embed/' + videoId[1] + '?&autoplay=1" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>');
	});
}

DIGIO._currencyFormat = function($val, symbol) {
	return symbol + ' ' + parseFloat($val).toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
}

DIGIO._calculationPlots = function($valorFinanciado, $parcelas, $juros) {
	var result = $valorFinanciado * Math.pow((1 + ($juros/100)), $parcelas) / 100;
	return result.toFixed(2);
}

DIGIO._setValues = function() {
	var $scope  = $('*[data-scope="simulation-form"]:first'),
		$range  = $('*[data-item="range"]:first', $scope),
		$value  = $('*[data-item="value"]:first', $scope),
		$times  = $('*[data-item="times"]:first', $scope),
		$tax    = $('*[data-item="tax"]:first', $scope).html(),
		$result = $('*[data-item="result"]:first', $scope);

	localStorage.setItem('value',  $range.val());
	localStorage.setItem('times', parseFloat($times.find('input[name="plots"]:checked').val()));
	localStorage.setItem('tax', parseFloat($tax));

	$value.text(DIGIO._currencyFormat(localStorage.getItem('value'), 'R$'));

	var time  = localStorage.getItem('times'),
		value = localStorage.getItem('value'),
		tax   = localStorage.getItem('tax');

	$result.text(localStorage.getItem('times') + 'x R$ ' + DIGIO._calculationPlots(value, time, tax));

}

DIGIO.calcFormSimulation = function() {
	var $scope = $('*[data-scope="simulation-form"]:first'),
		$range = $('*[data-item="range"]:first', $scope),
		$value = $('*[data-item="value"]:first', $scope),
		$times = $('*[data-item="times"]:first', $scope);

	DIGIO._setValues();

	$range.on('input', function(e){
		var valToColor = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));
		$(this).css('background-image',
			'-webkit-gradient(linear, left top, right top, '
			+ 'color-stop(' + valToColor + ', #00A0E6), '
			+ 'color-stop(' + valToColor + ', #C8CCCE)'
			+ ')'
		);

		DIGIO._setValues();
	});

	$times.change(function() {
        DIGIO._setValues();
    });
}

DIGIO.mobile_menu = function() {
	var $scope      = $('*[data-scope="header"]:first'),
		$mobileMenu = $('*[data-item="mobile-menu"]:first', $scope);

	$('.menu-button').on('click', function(e) {
		e.preventDefault();
		$('html,body').toggleClass('nav-open');
	});
}

DIGIO.masks = function() {
	$('*[data-mask="date"]').mask('99/99/9999');
	$('*[data-mask="cpf"]').mask('999.999.999-99');

	$('*[data-mask="phone"]').focusout(function(){
		var phone, element;
		element = $(this);
		element.unmask();
		phone = element.val().replace(/\D/g, '');
		if(phone.length > 10) {
			element.mask("(99) 99999-999?9");
		} else {
			element.mask("(99) 9999-9999?9");
		}
	}).trigger('focusout');
}